package com.example.hungamavideoapp.adapter;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.hungamavideoapp.Constant;
import com.example.hungamavideoapp.R;
import com.example.hungamavideoapp.models.MovieDetailResponse;
import com.example.hungamavideoapp.utils.HungamaTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieHolder> implements Filterable {

    private ArrayList<MovieDetailResponse> movieResponseFilteredList;
    private ArrayList<MovieDetailResponse>movieResultResponseArrayList;
    private MovieAdapter.OnVideoPlayClickListener onVideoPlayClickListener;

    public MovieAdapter(ArrayList<MovieDetailResponse> movieResponseArrayList) {
        this.movieResultResponseArrayList = movieResponseArrayList;
        this.movieResponseFilteredList = movieResponseArrayList;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MovieHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_movie,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
         holder.bindView(position, movieResponseFilteredList.get(position),onVideoPlayClickListener);
    }

    @Override
    public int getItemCount() {
        return movieResponseFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    movieResponseFilteredList = movieResultResponseArrayList;
                } else {
                    Log.d("filterString", "performFiltering: "+charString);
                    ArrayList<MovieDetailResponse> filteredList = new ArrayList<>();
                    for (MovieDetailResponse movieResultResponse : movieResultResponseArrayList) {
                        if (movieResultResponse.getOriginalTitle().toLowerCase().charAt(0) ==
                                charString.toLowerCase().charAt(0)){
                            filteredList.add(movieResultResponse);
                        }
                    }

                    movieResponseFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = movieResponseFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                movieResponseFilteredList = (ArrayList<MovieDetailResponse>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


    static class MovieHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.movieImg)
        AppCompatImageView movieImg;

        @BindView(R.id.movieTitleTxt)
        HungamaTextView movieTitleTxt;

        @BindView(R.id.movieReleaseDateTxt)
        HungamaTextView movieReleaseDateTxt;

        @BindView(R.id.movieDetailsTxt)
        HungamaTextView movieDetailsTxt;

        @BindView(R.id.moviePlay)
        HungamaTextView moviePlay;





        private MovieHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }

       private void  bindView(int position, MovieDetailResponse movieResponse,
                              MovieAdapter.OnVideoPlayClickListener onVideoPlayClickListener){
            movieTitleTxt.setText(movieResponse.getOriginalTitle());
            movieReleaseDateTxt.setText(movieResponse.getReleaseDate());
            movieDetailsTxt.setText(movieResponse.getOverview());

            moviePlay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                  onVideoPlayClickListener.onVideoPlayClick(position,movieResponse.getId());
                }
            });

           try {

               Glide.with(movieImg.getContext()).load
                       (Constant.VIDEO_IMG_URL+movieResponse.getPosterPath()).apply(new RequestOptions()
                       .placeholder(R.mipmap.ic_launcher_round)).
                       listener(new RequestListener<Drawable>() {
                           @Override
                           public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                               return false;
                           }

                           @Override
                           public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                               return false;
                           }
                       }).into(movieImg);


           } catch (Exception e) {
               e.printStackTrace();
           }
        }
    }

    public void setOnItemClickListener(MovieAdapter.OnVideoPlayClickListener onItemClickListener){
        this.onVideoPlayClickListener = onItemClickListener;

    }

   public interface OnVideoPlayClickListener{
       void onVideoPlayClick(int position,int movieId);
   }

}
