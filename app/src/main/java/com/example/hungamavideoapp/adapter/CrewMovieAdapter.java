package com.example.hungamavideoapp.adapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.hungamavideoapp.Constant;
import com.example.hungamavideoapp.R;
import com.example.hungamavideoapp.models.CrewMovieResponse;
import com.example.hungamavideoapp.utils.HungamaTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CrewMovieAdapter  extends RecyclerView.Adapter<CrewMovieAdapter.CrewMovieHolder>{

    private ArrayList<CrewMovieResponse> crewMovieResultArrayList;

    public CrewMovieAdapter(ArrayList<CrewMovieResponse> crewMovieResults) {
        this.crewMovieResultArrayList = crewMovieResults;
    }

    @NonNull
    @Override
    public CrewMovieAdapter.CrewMovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CrewMovieAdapter.CrewMovieHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_cast,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull CrewMovieAdapter.CrewMovieHolder holder, int position) {
        holder.bindView(crewMovieResultArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return crewMovieResultArrayList.size();
    }

    static class CrewMovieHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.movieCastName)
        HungamaTextView movieCrewName;

        @BindView(R.id.imgCastMovie)
        AppCompatImageView imgCrewMovie;

        public CrewMovieHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        void bindView(CrewMovieResponse crewMovieResult){
            movieCrewName.setText(crewMovieResult.getName());
            try {

                Glide.with(imgCrewMovie.getContext()).load
                        (Constant.VIDEO_IMG_URL+crewMovieResult.getLogoPath()).apply(new RequestOptions()
                        .placeholder(R.mipmap.ic_launcher_round)).
                        listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).into(imgCrewMovie);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
