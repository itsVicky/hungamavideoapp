package com.example.hungamavideoapp.adapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.hungamavideoapp.Constant;
import com.example.hungamavideoapp.R;
import com.example.hungamavideoapp.models.MovieCastResponse;
import com.example.hungamavideoapp.utils.HungamaTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieCastAdapter extends RecyclerView.Adapter<MovieCastAdapter.MovieCastHolder> {

    private ArrayList<MovieCastResponse> movieCastArrayList;

    public MovieCastAdapter(ArrayList<MovieCastResponse> movieCastArrayList) {
        this.movieCastArrayList = movieCastArrayList;
    }

    @NonNull
    @Override
    public MovieCastHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MovieCastHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_cast,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MovieCastHolder holder, int position) {
      holder.bindView(movieCastArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return movieCastArrayList.size();
    }

    static class MovieCastHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.movieCastName)
        HungamaTextView movieCastName;

        @BindView(R.id.imgCastMovie)
        AppCompatImageView imgCastMovie;

        public MovieCastHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        void bindView(MovieCastResponse movieCastResponse){
            movieCastName.setText(movieCastResponse.getName());

            try {

                Glide.with(imgCastMovie.getContext()).load
                        (Constant.VIDEO_IMG_URL+movieCastResponse.getProfilePath()).apply(new RequestOptions()
                        .placeholder(R.mipmap.ic_launcher_round)).
                        listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).into(imgCastMovie);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
