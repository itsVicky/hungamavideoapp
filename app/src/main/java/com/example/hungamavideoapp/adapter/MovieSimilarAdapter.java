package com.example.hungamavideoapp.adapter;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.hungamavideoapp.Constant;
import com.example.hungamavideoapp.R;
import com.example.hungamavideoapp.models.SimilarMovieCastResponse;
import com.example.hungamavideoapp.utils.HungamaTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieSimilarAdapter extends RecyclerView.Adapter<MovieSimilarAdapter.MovieSimilarHolder> {

    private ArrayList<SimilarMovieCastResponse> similarResultArrayList;

    public MovieSimilarAdapter(ArrayList<SimilarMovieCastResponse> similarResultArrayList) {
        this.similarResultArrayList = similarResultArrayList;
    }

    @NonNull
    @Override
    public MovieSimilarAdapter.MovieSimilarHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MovieSimilarAdapter.MovieSimilarHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_cast,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MovieSimilarAdapter.MovieSimilarHolder holder, int position) {
        holder.bindView(similarResultArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return similarResultArrayList.size();
    }

    static class MovieSimilarHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.movieCastName)
        HungamaTextView movieCastName;

        @BindView(R.id.imgCastMovie)
        AppCompatImageView imgSimilarMovie;

        public MovieSimilarHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }

        void bindView(SimilarMovieCastResponse similarResult){
            movieCastName.setText(similarResult.getTitle());
            try {

                Glide.with(imgSimilarMovie.getContext()).load
                        (Constant.VIDEO_IMG_URL+similarResult.getBackDropPath()).apply(new RequestOptions()
                        .placeholder(R.mipmap.ic_launcher_round)).
                        listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        }).into(imgSimilarMovie);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
