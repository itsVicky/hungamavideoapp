package com.example.hungamavideoapp.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.Nullable;

public class HungamaTextView extends TextView {
    public HungamaTextView(Context context) {
        super(context);

        CustomFont.applyCustomFont(this,context,null);
    }

    public HungamaTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        CustomFont.applyCustomFont(this,context,attrs);

    }

    public HungamaTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        CustomFont.applyCustomFont(this,context,attrs);

    }


}
