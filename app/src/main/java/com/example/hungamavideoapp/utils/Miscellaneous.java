package com.example.hungamavideoapp.utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.SyncStateContract;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;

import com.example.hungamavideoapp.R;


public class Miscellaneous {

    private static final String NO_ADDRESS = "No address associated with hostname";
    private static final String GAI_EXCEPTION = "android.system.GaiException";

    public static boolean isNetworkAvailable(Activity activity, boolean showToast){
        boolean isAvailable = isNetworkConnected(activity.getApplicationContext());
        if(!isAvailable){
            if(showToast)
                Toast.makeText(activity, R.string.network_unavailable,Toast.LENGTH_LONG).show();
        }
        return isAvailable;
    }




    private static boolean isNetworkConnected(Context applicationContext){
        ConnectivityManager connectivityManager = (ConnectivityManager) applicationContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null
                && activeNetworkInfo.isConnected();
    }

    public static boolean isUnableToResolveDomainResolved(Throwable throwable,Activity activity,boolean showToast){
        boolean isDomainResolved = isUnableToResolveDomain(throwable);
        if(isDomainResolved){
            if(showToast)
                Toast.makeText(activity, R.string.network_unavailable,Toast.LENGTH_LONG).show();
        }else {
            if(showToast)
                Toast.makeText(activity, R.string.something_went_wrong,Toast.LENGTH_LONG).show();
        }
        return isDomainResolved;
    }

    private static boolean isUnableToResolveDomain(Throwable error) {
        return error != null && error.getCause() != null && (error.getCause().toString().contains(NO_ADDRESS) || error.getCause().toString().contains(GAI_EXCEPTION));
    }

    public static void showKeyboard(Activity activity, CustomAutoCompleteTextView appCompatEditText){
        InputMethodManager inputMethodManager = (InputMethodManager)
                activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(appCompatEditText, 0);

    }

    public static void hideSoftKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
