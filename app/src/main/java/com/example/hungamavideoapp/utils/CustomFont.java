package com.example.hungamavideoapp.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.hungamavideoapp.R;

public class CustomFont {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public static void applyCustomFont(TextView customFontTextView, Context context, AttributeSet attrs) {
        TypedArray attributeArray = context.obtainStyledAttributes(
                attrs,
                R.styleable.HungamaTextView);
        // String fontName = attributeArray.getString(R.styleable.TechConnectTextView_font);
        int textStyle = attributeArray.getInt(R.styleable.HungamaTextView_font_name, 0);
        if (textStyle == 0) {
            textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
        }
        Typeface customFont = selectTypeface(context,textStyle);
        customFontTextView.setTypeface(customFont);
        attributeArray.recycle();
    }

    private static Typeface selectTypeface(Context context, int textStyle) {
        switch (textStyle) {
            case Typeface.BOLD: // bold
                //return FontCache.getTypeface("OpenSans-Bold.ttf", context);
                return FontCache.getTypeface("SourceSansPro-Bold.ttf", context);
            case Typeface.ITALIC: // italic
                return FontCache.getTypeface("SourceSansPro-Italic.ttf", context);
            case Typeface.BOLD_ITALIC: // bold italic
                return FontCache.getTypeface("SourceSansPro-BoldItalic.ttf", context);
            case Typeface.NORMAL: // regular
            default:
                return FontCache.getTypeface("SourceSansPro-Regular.ttf", context);
        }
    }
}
