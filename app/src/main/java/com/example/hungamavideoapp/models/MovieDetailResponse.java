package com.example.hungamavideoapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieDetailResponse implements Parcelable {

    int id;
    String backDropPath,posterPath,originalTitle,originalLanguage,releaseDate,overview;

      public MovieDetailResponse(){

      }

    public MovieDetailResponse(Parcel in) {
        id = in.readInt();
        backDropPath = in.readString();
        posterPath = in.readString();
        originalTitle = in.readString();
        originalLanguage = in.readString();
        releaseDate = in.readString();
        overview = in.readString();
    }

    public static final Creator<MovieDetailResponse> CREATOR = new Creator<MovieDetailResponse>() {
        @Override
        public MovieDetailResponse createFromParcel(Parcel in) {
            return new MovieDetailResponse(in);
        }

        @Override
        public MovieDetailResponse[] newArray(int size) {
            return new MovieDetailResponse[size];
        }
    };

    @Override
    public int describeContents() {

        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(backDropPath);
        parcel.writeString(posterPath);
        parcel.writeString(originalTitle);
        parcel.writeString(originalLanguage);
        parcel.writeString(releaseDate);
        parcel.writeString(overview);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBackDropPath() {
        return backDropPath;
    }

    public void setBackDropPath(String backDropPath) {
        this.backDropPath = backDropPath;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public static Creator<MovieDetailResponse> getCREATOR() {
        return CREATOR;
    }


    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    @Override
    public String toString() {
        return "MovieVideo{" +
                "id=" + id +
                ", backDropPath='" + backDropPath + '\'' +
                ", posterPath='" + posterPath + '\'' +
                ", originalTitle='" + originalTitle + '\'' +
                ", originalLanguage='" + originalLanguage + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                '}';
    }
}
