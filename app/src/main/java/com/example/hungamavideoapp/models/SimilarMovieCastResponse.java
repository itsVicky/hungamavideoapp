package com.example.hungamavideoapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class SimilarMovieCastResponse implements Parcelable {
    int similarMovieId;
    String backDropPath,title;

    public SimilarMovieCastResponse(){

    }

    protected SimilarMovieCastResponse(Parcel in) {
        similarMovieId = in.readInt();
        backDropPath = in.readString();
        title = in.readString();
    }

    public static final Creator<SimilarMovieCastResponse> CREATOR = new Creator<SimilarMovieCastResponse>() {
        @Override
        public SimilarMovieCastResponse createFromParcel(Parcel in) {
            return new SimilarMovieCastResponse(in);
        }

        @Override
        public SimilarMovieCastResponse[] newArray(int size) {
            return new SimilarMovieCastResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(similarMovieId);
        parcel.writeString(backDropPath);
        parcel.writeString(title);
    }

    public int getSimilarMovieId() {
        return similarMovieId;
    }

    public void setSimilarMovieId(int similarMovieId) {
        this.similarMovieId = similarMovieId;
    }

    public String getBackDropPath() {
        return backDropPath;
    }

    public void setBackDropPath(String backDropPath) {
        this.backDropPath = backDropPath;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public static Creator<SimilarMovieCastResponse> getCREATOR() {
        return CREATOR;
    }

    @Override
    public String toString() {
        return "SimilarMovieCast{" +
                "similarMovieId=" + similarMovieId +
                ", backDropPath='" + backDropPath + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
