package com.example.hungamavideoapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieCastResponse implements Parcelable {
    int castId;
    String name,ProfilePath;

    public MovieCastResponse(){

    }

    protected MovieCastResponse(Parcel in) {
        castId = in.readInt();
        name = in.readString();
        ProfilePath = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(castId);
        dest.writeString(name);
        dest.writeString(ProfilePath);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<MovieCastResponse> CREATOR = new Creator<MovieCastResponse>() {
        @Override
        public MovieCastResponse createFromParcel(Parcel in) {
            return new MovieCastResponse(in);
        }

        @Override
        public MovieCastResponse[] newArray(int size) {
            return new MovieCastResponse[size];
        }
    };

    public int getCastId() {
        return castId;
    }

    public void setCastId(int castId) {
        this.castId = castId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePath() {
        return ProfilePath;
    }

    public void setProfilePath(String profilePath) {
        ProfilePath = profilePath;
    }

    public static Creator<MovieCastResponse> getCREATOR() {
        return CREATOR;
    }

    @Override
    public String toString() {
        return "MovieCast{" +
                "castId=" + castId +
                ", name='" + name + '\'' +
                ", ProfilePath='" + ProfilePath + '\'' +
                '}';
    }
}
