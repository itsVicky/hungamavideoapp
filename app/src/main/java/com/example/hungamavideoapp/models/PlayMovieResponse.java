package com.example.hungamavideoapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class PlayMovieResponse implements Parcelable {
    int id;
    String videoKey;


    public PlayMovieResponse(){

    }

    protected PlayMovieResponse(Parcel in) {
        id = in.readInt();
        videoKey = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(videoKey);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PlayMovieResponse> CREATOR = new Creator<PlayMovieResponse>() {
        @Override
        public PlayMovieResponse createFromParcel(Parcel in) {
            return new PlayMovieResponse(in);
        }

        @Override
        public PlayMovieResponse[] newArray(int size) {
            return new PlayMovieResponse[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVideoKey() {
        return videoKey;
    }

    public void setVideoKey(String videoKey) {
        this.videoKey = videoKey;
    }

    public static Creator<PlayMovieResponse> getCREATOR() {
        return CREATOR;
    }

    @Override
    public String toString() {
        return "PlayMovieResponse{" +
                "id=" + id +
                ", videoKey='" + videoKey + '\'' +
                '}';
    }
}
