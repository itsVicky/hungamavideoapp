package com.example.hungamavideoapp.models;

import android.os.Parcel;
import android.os.Parcelable;

public class CrewMovieResponse implements Parcelable {
    String logoPath,name;

    public CrewMovieResponse(){

    }

    protected CrewMovieResponse(Parcel in) {
        logoPath = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(logoPath);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CrewMovieResponse> CREATOR = new Creator<CrewMovieResponse>() {
        @Override
        public CrewMovieResponse createFromParcel(Parcel in) {
            return new CrewMovieResponse(in);
        }

        @Override
        public CrewMovieResponse[] newArray(int size) {
            return new CrewMovieResponse[size];
        }
    };

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Creator<CrewMovieResponse> getCREATOR() {
        return CREATOR;
    }

    @Override
    public String toString() {
        return "CrewMovie{" +
                "logoPath='" + logoPath + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
