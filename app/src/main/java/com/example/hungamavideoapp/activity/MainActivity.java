package com.example.hungamavideoapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.example.hungamavideoapp.R;
import com.example.hungamavideoapp.adapter.MovieAdapter;

import com.example.hungamavideoapp.models.MovieDetailResponse;
import com.example.hungamavideoapp.models.PlayMovieResponse;
import com.example.hungamavideoapp.utils.CustomAutoCompleteTextView;
import com.example.hungamavideoapp.utils.Miscellaneous;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {



    private MovieAdapter movieAdapter;
    private Unbinder unbinder;

    private ArrayList<MovieDetailResponse>movieVideoArrayList = new ArrayList<>();
    private ArrayList<PlayMovieResponse>playMovieResponseArrayList = new ArrayList<>();
    public int movieVideoId;



   @BindView(R.id.mainProgressBar)
    ConstraintLayout mainProgressBar;

    @BindView(R.id.movieRecyclerView)
    RecyclerView movieRecyclerView;

    @BindView(R.id.searchQuery)
    CustomAutoCompleteTextView searchQuery;

    @BindView(R.id.serachIc)
    AppCompatImageView serachIc;

    @BindView(R.id.txt_search_layout)
    FrameLayout txtSearchLayout;

    @BindView(R.id.userCancelIcon)
    AppCompatImageView userCancelIcon;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);
        if (Miscellaneous.isNetworkAvailable(this, true)) {

            VideoRequest videoRequest = new VideoRequest();
            videoRequest.execute();
            initialMovieAdapter();
        }



    }




    @OnClick(R.id.serachIc)
    public void icSearch(){
        txtSearchLayout.setVisibility(View.VISIBLE);
        serachIc.setVisibility(View.GONE);
        Miscellaneous.showKeyboard(this,searchQuery);
    }

    @OnClick(R.id.userCancelIcon)
    public void cancelIc(){
        txtSearchLayout.setVisibility(View.GONE);
        serachIc.setVisibility(View.VISIBLE);
        searchQuery.getText().clear();
        Miscellaneous.hideSoftKeyboard(this);
    }

    private void initialMovieAdapter(){
        movieAdapter = new MovieAdapter(movieVideoArrayList);
        movieRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        movieRecyclerView.setAdapter(movieAdapter);
        movieAdapter.setOnItemClickListener(new MovieAdapter.OnVideoPlayClickListener() {
            @Override
            public void onVideoPlayClick(int position, int movieId) {
                new ShowVideoRequest(position).execute();



                Log.d("movieId", "onVideoPlayClick: "+movieVideoId);
            }
        });




        searchQuery.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               movieAdapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }



    class ShowVideoRequest  extends AsyncTask<Void,Void,String>{
        int position;
        String apiKey;

       ShowVideoRequest(int position){
           this.position = position;
       }

        @Override
        protected String doInBackground(Void... ints) {
            String result = null;
            try {
                URL url = new URL("https://api.themoviedb.org/3/movie/"+movieVideoArrayList.get(position).getId()+
                        "/videos?api_key=cdefdab512224d8836c8bac0e6813205&language=en-US"
                );
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = inputStreamToString(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject  = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("results");
                String videoCode = jsonArray.getJSONObject(0).getString("key");

                Intent intent = new Intent(MainActivity.this,MovieVideoActivity.class);
                intent.putExtra("videoId",movieVideoArrayList.get(position).getId());
                intent.putExtra("movieTitle",movieVideoArrayList.get(position).getOriginalTitle());
                intent.putExtra("overview",movieVideoArrayList.get(position).getOverview());
                intent.putExtra("release_date",movieVideoArrayList.get(position).getReleaseDate());
                intent.putExtra("language",movieVideoArrayList.get(position).getOriginalLanguage());
                intent.putExtra("videoKey",videoCode);
                Log.d("yotubecode", "onResponse: "+videoCode);
                startActivity(intent);
                if (txtSearchLayout.getVisibility()==View.VISIBLE){
                    searchQuery.getText().clear();


                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }


     class VideoRequest extends AsyncTask<Void,Void,String>{

         @Override
         protected void onPreExecute() {
             super.onPreExecute();
             mainProgressBar.setVisibility(View.VISIBLE);

         }

         @Override
         protected String doInBackground(Void... voids) {
             String result = null;
             try {
                 URL url = new URL("https://api.themoviedb.org/3/movie/now_playing?api_key=cdefdab512224d8836c8bac0e6813205&language=en-US&page=1");
                 HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                 InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                 result = inputStreamToString(in);
             } catch (Exception e) {
                 e.printStackTrace();
             }
             return result;
         }

         @Override
         protected void onPostExecute(String s) {
             super.onPostExecute(s);
             mainProgressBar.setVisibility(View.GONE);
             try {
                 JSONObject jsonObject  = new JSONObject(s);
                 JSONArray jsonArray = jsonObject.getJSONArray("results");

                 for (int i = 0 ; i<jsonArray.length();i++){
                     MovieDetailResponse movieVideo = new MovieDetailResponse();
                     int id = jsonArray.getJSONObject(i).getInt("id");
                     String movieName = jsonArray.getJSONObject(i).getString("title");
                     movieVideo.setId(jsonArray.getJSONObject(i).getInt("id"));
                     movieVideo.setOriginalTitle(jsonArray.getJSONObject(i).getString("title"));
                     movieVideo.setBackDropPath(jsonArray.getJSONObject(i).getString("backdrop_path"));
                     movieVideo.setPosterPath(jsonArray.getJSONObject(i).getString("poster_path"));
                     movieVideo.setReleaseDate(jsonArray.getJSONObject(i).getString("release_date"));
                     movieVideo.setOverview(jsonArray.getJSONObject(i).getString("overview"));
                     movieVideoArrayList.add(movieVideo);
                     Log.d("TAG", "onPostExecute: "+id + "&&&&"+movieName);
                 }

                 movieAdapter.notifyDataSetChanged();

             }catch (Exception e){
                 e.printStackTrace();
             }
         }
     }

    private String inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();

        InputStreamReader isr = new InputStreamReader(is);

        BufferedReader rd = new BufferedReader(isr);

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer.toString();
    }




    @Override
    public void onDestroy() {
        super.onDestroy();
        if(unbinder!=null)
            unbinder.unbind();

    }
}
