package com.example.hungamavideoapp.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.hungamavideoapp.Constant;
import com.example.hungamavideoapp.R;
import com.example.hungamavideoapp.adapter.CrewMovieAdapter;
import com.example.hungamavideoapp.adapter.MovieCastAdapter;
import com.example.hungamavideoapp.adapter.MovieSimilarAdapter;
import com.example.hungamavideoapp.models.CrewMovieResponse;
import com.example.hungamavideoapp.models.MovieCastResponse;
import com.example.hungamavideoapp.models.SimilarMovieCastResponse;
import com.example.hungamavideoapp.utils.HungamaTextView;
import com.example.hungamavideoapp.utils.Miscellaneous;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MovieVideoActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    @BindView(R.id.youtubeView)
    YouTubePlayerView youtubeView;

    @BindView(R.id.movieCastRecycler)
    RecyclerView movieCastRecycler;

    @BindView(R.id.movieCrewRecycler)
    RecyclerView movieCrewRecycler;

    @BindView(R.id.movieSimilarRecyclerView)
    RecyclerView movieSimilarRecyclerView;

    @BindView(R.id.movieTitleTxt)
    HungamaTextView movieTitleTxt;

    @BindView(R.id.movieReleaseDateTxt)
    HungamaTextView movieReleaseDateTxt;

    @BindView(R.id.movieLngTxt)
    HungamaTextView movieLngTxt;

    @BindView(R.id.movieDescTxt)
    HungamaTextView movieDescTxt;

    @BindView(R.id.mainProgressBar)
    ConstraintLayout mainProgressBar;

    private Unbinder unbinder;



    private MovieCastAdapter movieCastAdapter;
    private MovieSimilarAdapter movieSimilarAdapter;
    private CrewMovieAdapter crewMovieAdapter;



    private ArrayList<MovieCastResponse>castArrayList = new ArrayList<>();
    private ArrayList<SimilarMovieCastResponse>similarMovieCastArrayList = new ArrayList<>();
    private ArrayList<CrewMovieResponse>crewMovieArrayList = new ArrayList<>();


    private int movieId;
    private String movieTitle, releaseDate, overView, language, videoCode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hungama_video);
        unbinder = ButterKnife.bind(this);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {
            movieId = intent.getIntExtra("videoId", 0);
            movieTitle = intent.getStringExtra("movieTitle");
            releaseDate = intent.getStringExtra("release_date");
            overView = intent.getStringExtra("overview");
            language = intent.getStringExtra("language");
            videoCode = intent.getStringExtra("videoKey");


            movieTitleTxt.setText(movieTitle);
            movieReleaseDateTxt.setText(releaseDate);
            movieDescTxt.setText(overView);
            movieLngTxt.setText(language);
        }

        youtubeView.initialize(Constant.VIDEO_API_KEY, this);



        if (Miscellaneous.isNetworkAvailable(this, true)) {
            new MovieCastRequest().execute();
            initializeAdapters();
        }

    }




    class MovieCastRequest extends AsyncTask<Void,String,String>{

        @Override
        protected String doInBackground(Void... voids) {
            String result = null;
            try {
                URL url = new URL("https://api.themoviedb.org/3/movie/"+movieId+"/credits?api_key=cdefdab512224d8836c8bac0e6813205"
                );
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = inputStreamToString(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("cast");
                for (int i = 0 ; i<jsonArray.length();i++){
                    MovieCastResponse movieCast = new MovieCastResponse();
                    movieCast.setCastId(jsonArray.getJSONObject(i).getInt("cast_id"));
                    movieCast.setName(jsonArray.getJSONObject(i).getString("name"));
                    movieCast.setProfilePath(jsonArray.getJSONObject(i).getString("profile_path"));
                    castArrayList.add(movieCast);
                }

                movieCastAdapter.notifyDataSetChanged();

                new CrewMovieRequest().execute();

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    private String inputStreamToString(InputStream is) {
        String rLine = "";
        StringBuilder answer = new StringBuilder();

        InputStreamReader isr = new InputStreamReader(is);

        BufferedReader rd = new BufferedReader(isr);

        try {
            while ((rLine = rd.readLine()) != null) {
                answer.append(rLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return answer.toString();
    }





    class SimilarRequest extends AsyncTask<Void,String,String>{

        @Override
        protected String doInBackground(Void... voids) {
            String result = null;
            try {
                URL url = new URL("https://api.themoviedb.org/3/movie/"+movieId+"/similar?api_key=cdefdab512224d8836c8bac0e6813205&language=en-US&page=1"
                );
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = inputStreamToString(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("results");
                for (int i = 0 ; i < jsonArray.length();i++){
                     SimilarMovieCastResponse similarMovieCast = new SimilarMovieCastResponse();
                     similarMovieCast.setSimilarMovieId(jsonArray.getJSONObject(i).getInt("id"));
                     similarMovieCast.setBackDropPath(jsonArray.getJSONObject(i).getString("backdrop_path"));
                     similarMovieCast.setTitle(jsonArray.getJSONObject(i).getString("original_title"));
                     similarMovieCastArrayList.add(similarMovieCast);
                }
                movieSimilarAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }



    class CrewMovieRequest extends AsyncTask<Void,String,String>{

        @Override
        protected String doInBackground(Void... voids) {
            String result = null;
            try {
                URL url = new URL("https://api.themoviedb.org/3/movie/"+movieId+"?api_key=cdefdab512224d8836c8bac0e6813205&language=en-US");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                result = inputStreamToString(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                JSONObject jsonObject = new JSONObject(s);
                JSONArray jsonArray = jsonObject.getJSONArray("production_companies");
                for (int i = 0 ; i < jsonArray.length();i++){
                    CrewMovieResponse crewMovie = new CrewMovieResponse();
                    crewMovie.setName(jsonArray.getJSONObject(i).getString("name"));
                    crewMovie.setLogoPath(jsonArray.getJSONObject(i).getString("logo_path"));
                    crewMovieArrayList.add(crewMovie);
                }
                crewMovieAdapter.notifyDataSetChanged();

                new SimilarRequest().execute();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private void initializeAdapters() {
        movieCastAdapter = new MovieCastAdapter(castArrayList);
        movieCastRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        movieCastRecycler.setAdapter(movieCastAdapter);

        crewMovieAdapter = new CrewMovieAdapter(crewMovieArrayList);
        movieCrewRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        movieCrewRecycler.setAdapter(crewMovieAdapter);

        movieSimilarAdapter = new MovieSimilarAdapter(similarMovieCastArrayList);
        movieSimilarRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        movieSimilarRecyclerView.setAdapter(movieSimilarAdapter);


    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
                                        YouTubePlayer player, boolean wasRestored) {

        if (!wasRestored) {
            player.loadVideo(videoCode);
            player.setPlaybackEventListener(playbackEventListener);
            player.setPlayerStateChangeListener(playerStateChangeListener);


        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    private YouTubePlayer.PlaybackEventListener playbackEventListener = new YouTubePlayer.PlaybackEventListener() {

        @Override
        public void onBuffering(boolean arg0) {
        }

        @Override
        public void onPaused() {
        }

        @Override
        public void onPlaying() {
        }

        @Override
        public void onSeekTo(int arg0) {
        }

        @Override
        public void onStopped() {
        }

    };

    private YouTubePlayer.PlayerStateChangeListener playerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {

        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }

        @Override
        public void onLoaded(String arg0) {
        }

        @Override
        public void onLoading() {
        }

        @Override
        public void onVideoEnded() {

        }

        @Override
        public void onVideoStarted() {
        }
    };


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null)
            unbinder.unbind();


    }
}
